package com.retrofitgenericsample.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import static com.retrofitgenericsample.api.Api.GETUSER;

/**
 * Created by root on 22/3/18.
 */

public interface GithubService {

    @GET(GETUSER)
    Call<User> getUser(@Path("username") String username);


}
