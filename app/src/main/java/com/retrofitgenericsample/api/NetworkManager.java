package com.retrofitgenericsample.api;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;

import com.retrofitgenericsample.R;
import com.retrofitgenericsample.RetrofitGenericApplication;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 22/3/18.
 */

public class NetworkManager {

    private GithubService githubService;
    private static NetworkManager networkManager;

    private NetworkManager() {
        githubService = ServiceGenerator.createService(GithubService.class);
    }


    public static NetworkManager getInstance() {
        if (networkManager == null)
            networkManager = new NetworkManager();
        return networkManager;
    }


    public void getUser(String username, NetworkResponseListener listner) {
//        call = githubService.getUser(username);
        Call<User> call = githubService.getUser(username);
        callRetrofit(Api.GETUSER, listner, call);

    }

    public void cancelPreviousRequest() {
//        if (call != null) {
//            call.cancel();
//        }

    }

    private <T> void callRetrofit(final String ApiKey, final NetworkResponseListener listener, Call<T> call) {
        if (!validateIntenetContivity(ApiKey, listener))
            return;

        call.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                /*if(i==1){
                    User user = (User) response.body(); // use the user object for the other fields
                }else if (i==2){
                    Patient user = (Patient) response.body();
                }*/
                T object = response.body();

                if (object == null) {
                    //404 or the response cannot be converted to User.
                    ResponseBody responseBody = response.errorBody();
                    if (responseBody != null) {
                        try {

                            listener.apiError(ApiKey, responseBody.string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        listener.apiError(ApiKey, showServerMsg());
                    }
                } else {
                    listener.networkResponse(ApiKey, object);
                }


            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {
                listener.apiError(ApiKey, t.getLocalizedMessage());
            }
        });

    }

    public String showServerMsg() {
        return RetrofitGenericApplication.getApplicationInstance().getString(R.string.server_msg);
    }

    /**
     * @param listener generic Listener
     *                 purpose : The key web service will be used to generate key.
     */

    public static boolean validateIntenetContivity(String ApiKey, final NetworkResponseListener listener) {
        boolean isInternetPresent;
        isInternetPresent = isConnectingToInternet(RetrofitGenericApplication.getApplicationInstance().getApplicationContext());
        if (!isInternetPresent) {
            listener.apiError(ApiKey, showNoInternetConnection());
        }
        return isInternetPresent;
    }

    /**
     * Checking for all possible internet providers
     * *
     */
    @SuppressLint("MissingPermission")
    public static boolean isConnectingToInternet(Context mContext) {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        return mConnectivityManager.getActiveNetworkInfo() != null
                && mConnectivityManager.getActiveNetworkInfo().isAvailable()
                && mConnectivityManager.getActiveNetworkInfo().isConnected();
    }

    private static String showNoInternetConnection() {
        return RetrofitGenericApplication.getApplicationInstance().getString(R.string.check_internet);
    }

}
