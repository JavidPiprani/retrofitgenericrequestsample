package com.retrofitgenericsample.api;

/**
 * Created by Admin on 09-10-2015.
 */
public interface NetworkResponseListener {
    void networkResponse(String requested, Object response);

    void apiError(String requested, String errorMsg);

    void serverError(String requested, String errorMsg);
}