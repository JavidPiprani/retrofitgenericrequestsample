package com.retrofitgenericsample.api;

/**
 * Created by root on 22/3/18.
 */

public interface Api {

    int SUCCESS = 200;
    int UNSUCCESS = 500;
    int UNAUTHORIZED = 401;
    String GETUSER = "/users/{username}";

}
