package com.retrofitgenericsample.model;


import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.retrofitgenericsample.api.Api;

/**
 * Created by Admin on 17-11-2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "message"
})
public class BaseResponse {
//    https://github.com/basil2style/Retrofit-Android-Basic/blob/master/FlowerPI/app/src/main/java/com/makeinfo/flowerpi/model/GitHubModel.java


    @JsonProperty("status")
    public Integer status_code;
    @JsonProperty("message")
    public String message;
    public final static Parcelable.Creator<BaseResponse> CREATOR = new Parcelable.Creator<BaseResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BaseResponse createFromParcel(Parcel in) {
            BaseResponse instance = new BaseResponse();
            instance.status_code = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public BaseResponse[] newArray(int size) {
            return (new BaseResponse[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status_code);
        dest.writeValue(message);
    }

    public int describeContents() {
        return 0;
    }

    @JsonIgnore
    public void setErrorMessage(String errMsg) {
        this.message = errMsg;
        status_code = Api.UNSUCCESS;
    }

    @JsonIgnore
    public boolean isValid() {
        if (status_code == Api.SUCCESS)
            return true;
        else
            return false;
    }

    public boolean isUnAuthorized() {
        if (status_code == Api.UNAUTHORIZED && !TextUtils.isEmpty(this.message)) {
            return true;
        }
        return false;
    }
}
