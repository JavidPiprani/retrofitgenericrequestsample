package com.retrofitgenericsample;

import android.app.Application;

/**
 * Created by root on 22/3/18.
 */

public class RetrofitGenericApplication extends Application {

    private static RetrofitGenericApplication appInstance;


    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
    }

    public static RetrofitGenericApplication getApplicationInstance() {
        return appInstance;
    }
}
