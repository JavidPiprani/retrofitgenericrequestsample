package com.retrofitgenericsample;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.retrofitgenericsample.api.NetworkManager;
import com.retrofitgenericsample.api.NetworkResponseListener;

public class MainActivity extends AppCompatActivity {

    private CoordinatorLayout rootlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        rootlayout = (CoordinatorLayout) findViewById(R.id.crParent);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });

        NetworkManager.getInstance().getUser("octocat", new NetworkResponseListener() {
            @Override
            public void networkResponse(String requested, Object response) {
                showSnackBar(rootlayout, response.toString());
            }

            @Override
            public void apiError(String requested, String errorMsg) {
                showSnackBar(rootlayout, errorMsg);
            }

            @Override
            public void serverError(String requested, String errorMsg) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void showSnackBar(View parentView, String msg) {
        Snackbar snackbar = Snackbar
                .make(parentView, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
